<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DoutorsTableSeeder::class);
    }
}


class DoutorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('doutors')->insert([
            'nome' => 'Willian Matheus',
            'setor' => 'Acumputura',
            'preco' => 170,
            'email' => 'willianmatheuscm@gmail.com'
        ]);
        DB::table('doutors')->insert([
            'nome' => 'TESTE TESTE',
            'setor' => 'TESTE',
            'preco' => 180,
            'email' => 'TESTtestTs@gmail.com'
        ]);
    }
}
