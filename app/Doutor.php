<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doutor extends Model
{
    protected $fillable = ['id', 'nome', 'setor', 'email', 'preco'];

}
