<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doutor;

class DoutorController extends Controller
{
    public function index()
    {
       $doutors = Doutor::orderBy('nome')->get();
       return response()
              ->json($doutors, 200, [], JSON_PRETTY_PRINT);        
    }

    public function store(Request $request)
    { 
       $dados = $request->all();
       
       $inc = Doutor::create($dados);

       if ($inc) {
           return response()->json([$inc], 201);
       } else {
           return response()
                  ->json(['erro'=>'error_insert'],500);
       }
    }

    public function show($id)
    {
        $reg = Doutor::find($id);

        if ($reg) {
            return response()
                   ->json($reg, 200, [], JSON_PRETTY_PRINT);
        } else {
            return response()
                   ->json(['erro'=>'not found'], 404);
        }
    }

    public function update(Request $request, $id)
    {
        $reg = Doutor::find($id);

        if ($reg) {
            $dados = $request->all();

            $alt = $reg->update($dados);

            if ($alt) {
                return response()
                      ->json($reg, 200, [], JSON_PRETTY_PRINT);
            } else {
                return response()
                       ->json(['erro'=>'not update'], 500);
            }          
        } else {
            return response()
                   ->json(['erro'=>'not found'], 404);
        }
    }

    public function destroy($id)
    {
        $reg = Doutor::find($id);

        if ($reg) {
            if ($reg->delete()) {
                return response()
                      ->json(['msg' =>'Ok! Excluído'], 200);
            } else {
                return response()
                       ->json(['erro'=>'not delete'], 500);
            }      
        } else {
            return response()
                   ->json(['erro'=>'not found'], 404);
        }
    }
}
